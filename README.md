### Instructions for running this POC

1. Clone this branch
2. `cd gitlab-vscode-extension`
3. `npm ci`
4. `npm run package`
5. _ensure vsix file generated_
6. `cd test/wdio`
7. `npm install`
8. `TEST_GITLAB_TOKEN=<PAT with code suggestion access> npm run wdio`

Note: the first time wdio is run VSCode will be downloaded.
